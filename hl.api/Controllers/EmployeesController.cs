﻿using hl.api.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace hl.api.Controllers
{
    [RoutePrefix("api/employees")]
    public class EmployeesController : ApiController
    {
        // GET: api/Employees
        public IEnumerable<EmployeeNewHireView> Get()
        {
            return new EmployeeNewHireView[] {};
        }

        // GET: api/Employees/5
        public EmployeeNewHireView Get(string id)
        {
            if(!id.Equals("5"))
                throw new HttpException(404,"Not found");

            // mock
            EmployeeNewHireView view = new EmployeeNewHireView(){
                HLID = "5",
                LegalFirstName = "Andrew",
                LegaLastName = "Kupiec",
                LegalMiddleName = "David",
                Title = "Associate",
                StartDate = DateTime.Parse("2017-05-01"),
                Status = "Exempt",
                Office = "Los Angeles",
                Rehire = false,
                StaffType = "FIN",
                Phone = "N/A",
                Workstation = "TBD",
                Computer = "None",
                ResourcesNeeded = new string[] { "Workstation", 
                                                    "Network Logon", 
                                                    "Email Account",
                                                    "VoiceMail",
                                                    "Salesforce",
                                                    "Expense Report"
                                               },
                PDC = "O'Toole, Ryan",
                ExpenseApprover = "Yu, Christina",
                SubmittedBy = "Lazar, Ron",
                SubmittedDate = DateTime.Parse("2017-04-21")
            };
            
            return view;
        }

        static void Webhook(EmployeeModel m)
        {
            HttpRequestMessage req = new HttpRequestMessage(HttpMethod.Post, "http://requestb.in/v0qav7v0");
            req.Content = new StringContent(Newtonsoft.Json.JsonConvert.SerializeObject(m));
            HttpClient c = new HttpClient();
            c.SendAsync(req);
        }

        // POST: api/Employees
        public void Post([FromBody]EmployeeNewHireView value)
        {
            // trigger newhireevent
            if (!value.HLID.Equals("5"))
                throw new HttpException(404, "Not found");

            Webhook(value);
            
        }

        // PUT: api/Employees/5
        public void Put(int id, [FromBody]string value)
        {
            throw new NotImplementedException();
        }

        // DELETE: api/Employees/5
        public void Delete(int id)
        {
            throw new NotImplementedException();
        }
    }
}
