﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace hl.api.Models
{
    public enum EmployeeEventType
    {
        NewHire,
        Transfer,
        Departure,
        Update
    }
    
    public class EmployeeEvent<T> where T : hl.api.Models.EmployeeModel
    {
        public EmployeeEventType EventType { get; set; }

        public T Employee { get; set; }

    }
}