﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace hl.api.Models
{
    public class EmployeeNewHireView : EmployeeModel
    {
        public string HLID { get; set; }
        public string LegalFirstName { get; set; }
        public string LegalMiddleName { get; set; }
        public string LegaLastName { get; set; }
        public string Title { get; set; }
        public DateTime StartDate { get; set; }
        public string Classification { get; set; }
        public string Status { get; set; }
        public string Office { get; set; }
        public Boolean Rehire { get; set; }
        public string StaffType { get; set; }
        public string BUDepartment { get; set; }
        public string Allocations { get; set; }
        public string Phone { get; set; }
        public string Workstation { get; set; }
        public string Computer { get; set; }
        public IEnumerable<string> ResourcesNeeded { get; set; }
        public string PDC { get; set; }
        public string ExpenseApprover { get; set; }
        public string SubmittedBy { get; set; }
        public DateTime SubmittedDate { get; set; }

    }
}
